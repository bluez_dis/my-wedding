class UserGroup < ApplicationRecord
    has_and_belongs_to_many :action_models, join_table: "action_models_user_groups"
end
