class User < ApplicationRecord
  # Include default devise modules. Others available are:
  # :confirmable, :lockable, :timeoutable, :trackable and :omniauthable
  devise :database_authenticatable, :registerable,
         :recoverable, :rememberable, :validatable

  belongs_to :user_group

  extend FriendlyId
  friendly_id :name, use: :slugged
  
  def has_priviledge(controller_name, action_name)
    @not_authorized = true

    self.user_group.action_models.each do |action_model|
      if (action_model.controller_model.path == controller_name && action_model.path == action_name)
        @not_authorized = false
      end

      break if ! @not_authorized
    end

    if (! @not_authorized || self.user_group.super == 1)
      true
    else
      false 
    end
  end
end
