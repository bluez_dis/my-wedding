class ActionModel < ApplicationRecord
    belongs_to :controller_model
	has_and_belongs_to_many :user_groups, join_table: "action_models_user_groups"
end
