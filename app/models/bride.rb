class Bride < ApplicationRecord
    belongs_to :user, required: false, dependent: :destroy
    has_one_attached :image
end
