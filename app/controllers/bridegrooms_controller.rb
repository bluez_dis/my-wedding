class BridegroomsController < ApplicationController
    layout "application"

    def index

		@user = User.friendly.find(params[:id])
		@groom = Groom.find_by_user_id(@user.id)
		@bride = Bride.find_by_user_id(@user.id)		
		@weeding = Weeding.find_by_user_id(@user.id)
		# render json: @weeding
	end
end
