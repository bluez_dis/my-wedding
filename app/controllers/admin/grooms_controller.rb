class Admin::GroomsController < ApplicationController
    before_action :authenticate_user!
    # before_action :authorize_user
    before_action :set_groom, only: [:show, :edit, :update, :destroy]
    layout "admin"

    def index
        @groom = Groom.find_by_user_id(params[:user_id])
        @bride = Bride.find_by_user_id(params[:user_id])
        @weeding = Weeding.find_by_user_id(params[:user_id])
    end

    def new
        groom = Groom.new    

        respond_to do |format|
        format.html # new.html.erb
        format.xml  { render :xml => groom }
        end
    end

    def show

        respond_to do |format|
        format.html # show.html.erb
        format.xml  { render :xml => groom }
        end
    end

    def create
        groom = Groom.new(groom_params)

        respond_to do |format|
            if groom.save
                
                format.html { redirect_to(admin_groom_path(groom.id), :notice => 'Data pengantin pria telah di buat.') }
                format.xml  { render :xml => groom, :status => :created, :user => groom }
            else
                format.html { render :action => "new" }
                format.xml  { render :xml => groom.errors, :status => :unprocessable_entity }
            end
        end
    end

    def edit
        @bride = Bride.find_by_user_id(@groom.user_id)
    end

    def update

        respond_to do |format|
            if @groom.update_attributes(groom_params)

                bride = Bride.find(params[:bride_id])
                bride.name = params[:name_bride]
                bride.father_name = params[:father_name_bride]
                bride.mother_name = params[:mother_name_bride]
                bride.qoute = params[:qoute_bride]
                bride.save
                
                format.html { redirect_to(admin_grooms_path(user_id: @groom.user_id), :notice => 'Data pengantin pria / wanita telah di update.') }
                format.xml  { head :ok }
            else
                format.html { render :action => "edit" }
                format.xml  { render :xml => groom.errors, :status => :unprocessable_entity }
            end
        end
    end

    def destroy
        groom.destroy

        respond_to do |format|
            format.html { redirect_to(:controller => 'grooms') }
            format.xml  { head :ok }
        end                             
    end

    private

    # Use callbacks to share common setup or constraints between actions.
    def set_groom
        @groom = Groom.find(params[:id])
    end


    def groom_params
        params.require(:groom).permit(:name, :father_name, :mother_name, :qoute)
    end
end
