class Admin::UserGroupsController < ApplicationController
  before_action :authenticate_user!
  before_action :authorize_user
  before_action :set_user_group, only: [:show, :edit, :update, :destroy]
  layout "admin"
    
  def index
    @user_groups = UserGroup.order("name DESC")
  end
  
  def new
    @user_group = UserGroup.new
   
    respond_to do |format|
      format.html # new.html.erb
      format.xml  { render :xml => @user_group }
    end
  end
  
  def show 

    respond_to do |format|
      format.html # show.html.erb
      format.xml  { render :xml => @user_group }
    end
  end
  
  def create
    @user_group = UserGroup.new(user_group_params)
    
    respond_to do |format|
      if @user_group.save
        
        format.html { redirect_to(:controller => 'user_groups') }
        format.xml  { render :xml => @user_group, :status => :created, :user_group => @user_group }
      else
        format.html { render :action => "new" }
        format.xml  { render :xml => @user_group.errors, :status => :unprocessable_entity }
      end
    end
  end
  
  def edit 
  end
  
  def update 

    respond_to do |format|
      if @user_group.update_attributes(user_group_params)
        
        format.html { redirect_to(:controller => 'user_groups') }
        format.xml  { head :ok }
      else
        format.html { render :action => "edit" }
        format.xml  { render :xml => @user_group.errors, :status => :unprocessable_entity }
      end
    end
  end
  
  def destroy
    @user_group.destroy

    respond_to do |format|
      format.html { redirect_to(:controller => 'user_groups') }
      format.xml  { head :ok }
    end
  end

  private

  # Use callbacks to share common setup or constraints between actions.
  def set_user_group
    @user_group = UserGroup.find(params[:id])
  end


  def user_group_params
    params.require(:user_group).permit(:name, :super, :description, :created_by, :updated_by, action_model_ids: [])
  end
end
