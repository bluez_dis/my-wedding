class Admin::UsersController < ApplicationController
  before_action :authenticate_user!
  before_action :authorize_user
  before_action :set_user, only: [:show, :edit, :update, :destroy]
  layout "admin"

  def index
    @users = User.order("created_at DESC")
  end
  
  def new
    @user = User.new
    @user_groups = UserGroup.all
    
    respond_to do |format|
      format.html # new.html.erb
      format.xml  { render :xml => @user }
    end
  end
  
  def show

    respond_to do |format|
      format.html # show.html.erb
      format.xml  { render :xml => @user }
    end
  end
  
  def create
    @user = User.new(user_params)

    respond_to do |format|
      if @user.save

        if params[:title].to_i == 1 
          groom = Groom.new
          groom.name = @user.name
          groom.user_id = @user.id
          groom.save

          bride = Bride.new
          bride.user_id = @user.id
          bride.save          
        else
          bride = Bride.new
          bride.name = @user.name
          bride.user_id = @user.id
          bride.save

          groom = Groom.new
          groom.user_id = @user.id
          groom.save
        end  

        weeding = Weeding.new
        weeding.user_id = @user.id
        weeding.bride_id = bride.id
        weeding.groom_id = groom.id
        weeding.save

        format.html { redirect_to(admin_user_path(@user.id), :notice => 'user was successfully created.') }
        format.xml  { render :xml => @user, :status => :created, :user => @user }
      else
        format.html { render :action => "new" }
        format.xml  { render :xml => @user.errors, :status => :unprocessable_entity }
      end
    end
  end
  
  def edit
    @user_groups = UserGroup.all

  end
  
  def update

    respond_to do |format|
      if @user.update_attributes(user_params)
        
        format.html { redirect_to(admin_user_path(@user), :notice => 'user was successfully updated.') }
        format.xml  { head :ok }
      else
        format.html { render :action => "edit" }
        format.xml  { render :xml => @user.errors, :status => :unprocessable_entity }
      end
    end
  end
  
  def destroy
    @user.destroy

    respond_to do |format|
      format.html { redirect_to(:controller => 'users') }
      format.xml  { head :ok }
    end
  end

  private

  # Use callbacks to share common setup or constraints between actions.
  def set_user
    @user = User.friendly.find(params[:id])
  end


  def user_params
    params.require(:user).permit(:email, :name, :password_confirmation, :username, :name, :user_group_id, :password)
  end
end
