class Admin::WeedingPhotosController < ApplicationController
    before_action :authenticate_user!
    #before_action :authorize_user
    before_action :set_weeding_photo, only: [:show, :destroy]
    layout "admin"

    def new
        @weeding_photo = WeedingPhoto.new

        respond_to do |format|
            format.html # new.html.erb
            format.xml  { render :xml => @weeding_photo }
        end
    end

    def show 

        respond_to do |format|
            format.html # show.html.erb
            format.xml  { render :xml => @weeding_photo }
        end
    end

    def create
        @weeding_photo = WeedingPhoto.new(weeding_photo_params)

        respond_to do |format|
            if @weeding_photo.save
            
                format.html { redirect_to admin_grooms_path(user_id: current_user.id) , :notice => 'Data pengantin pria telah di buat.' }
                format.xml  { render :xml => @weeding_photo, :status => :created, :weeding => @weeding_photo }
            else
                format.html { render :action => "new" }
                format.xml  { render :xml => @weeding_photo.errors, :status => :unprocessable_entity }
            end
        end
    end

    def destroy
        @weeding_photo.destroy

        respond_to do |format|
            format.html { redirect_to admin_grooms_path(user_id: current_user.id) , :notice => 'Data pengantin pria telah di buat.' }
            format.xml  { head :ok }
        end
    end

    private

    # Use callbacks to share common setup or constraints between actions.
    def set_weeding_photo
        @weeding_photo = WeedingPhoto.find(params[:id])
    end


    def weeding_photo_params
        params.require(:weeding_photo).permit(:weeding_id, :order, :image)
    end
end
