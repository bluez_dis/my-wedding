class Admin::WeedingsController < ApplicationController
    before_action :authenticate_user!
    #before_action :authorize_user
    before_action :set_weeding, only: [:show, :edit, :update, :destroy]
    layout "admin"

    def index
        @weedings = Weeding.order("name DESC")
    end

    def new
        @weeding = Weeding.new

        respond_to do |format|
            format.html # new.html.erb
            format.xml  { render :xml => @weeding }
        end
    end

    def show 

        respond_to do |format|
            format.html # show.html.erb
            format.xml  { render :xml => @weeding }
        end
    end

    def create
        @weeding = Weeding.new(weeding_params)

        respond_to do |format|
            if @weeding.save
            
                format.html { redirect_to(:controller => 'weedings') }
                format.xml  { render :xml => @weeding, :status => :created, :weeding => @weeding }
            else
                format.html { render :action => "new" }
                format.xml  { render :xml => @weeding.errors, :status => :unprocessable_entity }
            end
        end
    end

    def edit 
    end

    def update 

        respond_to do |format|
            if @weeding.update_attributes(weeding_params)            
                format.html { redirect_to admin_grooms_path(user_id: current_user.id) , :notice => 'Data pengantin pria telah di buat.' }
                format.xml  { head :ok }
            else
                format.html { render :action => "edit" }
                format.xml  { render :xml => @weeding.errors, :status => :unprocessable_entity }
            end
        end
    end

    def destroy
        @weeding.destroy

        respond_to do |format|
            format.html { redirect_to(:controller => 'weedings') }
            format.xml  { head :ok }
        end
    end

    private

    # Use callbacks to share common setup or constraints between actions.
    def set_weeding
        @weeding = Weeding.find(params[:id])
    end


    def weeding_params
        params.require(:weeding).permit(:bride_id, :groom_id, :location_id, :start_date, :end_date, :start_time, :end_time, :number_guest)
    end
end
