class Admin::InvitedGuestsController < ApplicationController
    before_action :authenticate_user!
    # before_action :authorize_user
    before_action :set_invited_guest, only: [:show, :edit, :update, :destroy]
    layout "admin"

    def index
        @invited_guests = InvitedGuest.order("name DESC")
    end

    def new
        @invited_guest = InvitedGuest.new

        respond_to do |format|
            format.html # new.html.erb
            format.xml  { render :xml => @invited_guest }
        end
    end

    def show 

        respond_to do |format|
            format.html # show.html.erb
            format.xml  { render :xml => @invited_guest }
        end
    end

    def create
        @invited_guest = InvitedGuest.new(invited_guest_params)

        respond_to do |format|
            if @invited_guest.save
            
            format.html { redirect_to(:controller => 'invited_guests') }
            format.xml  { render :xml => @invited_guest, :status => :created, :invited_guest => @invited_guest }
            else
            format.html { render :action => "new" }
            format.xml  { render :xml => @invited_guest.errors, :status => :unprocessable_entity }
            end
        end
    end

    def edit 
    end

    def update 

        respond_to do |format|
            if @invited_guest.update_attributes(invited_guest_params)
            
            format.html { redirect_to(:controller => 'invited_guests') }
            format.xml  { head :ok }
            else
            format.html { render :action => "edit" }
            format.xml  { render :xml => @invited_guest.errors, :status => :unprocessable_entity }
            end
        end
    end

    def destroy
        @invited_guest.destroy

        respond_to do |format|
            format.html { redirect_to(:controller => 'invited_guests') }
            format.xml  { head :ok }
        end
    end

    private

    # Use callbacks to share common setup or constraints between actions.
    def set_invited_guest
        @invited_guest = InvitedGuest.find(params[:id])
    end


    def invited_guest_params
        params.require(:invited_guest).permit(:number, :name, :weeding_id, :phone, :email, :total_guest, :attend, :table_number, :invitation_has_been_send, :checkin, :start_invitation_hour, :end_invitation_hour)
    end
end
