class ApplicationController < ActionController::Base
    protect_from_forgery with: :exception
	protect_from_forgery with: :null_session

	layout :layout_by_resource
	
	before_action :controller_model


	  def controller_model
	    @months = ["", "Januari", "Februari", "Maret", "April", "Mei", "Juni", "Jul", "Agustus", "September", "Oktober", "November", "Desember"]
	    @days = ["Minggu", "Senin", "Selasa", "Rabu", "Kamis", "Jum'at", "Sabtu"]
	    Time.zone = "Jakarta"
	  end
	  
	  def layout_by_resource
		if devise_controller?
		  if resource_name == :user
			"login"
		  end
		else
		  "application"
		end
	   end
	  
	  def after_sign_in_path_for(resource_or_scope)
		  if resource_or_scope.is_a?(Admin)
		  admin_root_path    
		else
		  super
		end
	  end
	  
	  def after_sign_out_path_for(resource_or_scope)
		if resource_or_scope.to_s == "admin"
		  new_admin_session_path
		else
		  root_path
		end
	  end

	  def authorize_user
	    if current_user.has_priviledge(params[:controller], params[:action])
	      true
	    else
	      redirect_to admin_root_path, notice: "You're not authorized to access the page"
	    end
	  end
end
