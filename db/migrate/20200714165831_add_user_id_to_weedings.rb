class AddUserIdToWeedings < ActiveRecord::Migration[5.2]
  def change
    add_column :weedings, :user_id, :integer
  end
end
