class AddUserGroupIdToUsers < ActiveRecord::Migration[5.2]
  def change
      add_column :users, :user_group_id, :integer
  end
end
