class CreateWeedingPhotos < ActiveRecord::Migration[5.2]
  def change
    create_table :weeding_photos do |t|
      t.integer :weeding_id
      t.integer :order

      t.timestamps
    end
  end
end
