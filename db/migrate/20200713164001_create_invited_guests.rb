class CreateInvitedGuests < ActiveRecord::Migration[5.2]
  def change
    create_table :invited_guests do |t|
      t.integer :number
      t.integer :weeding_id
      t.string :name
      t.string :phone
      t.string :email
      t.integer :total_guest
      t.integer :attend, default: 0
      t.integer :table_number
      t.integer :invitation_has_been_send, default: 0
      t.integer :checkin, default: 0
      t.datetime :start_invitation_hour
      t.datetime :end_invitation_hour

      t.timestamps
    end
  end
end
