class CreateBrides < ActiveRecord::Migration[5.2]
  def change
    create_table :brides do |t|
      t.string :name
      t.string :mother_name
      t.string :father_name
      t.text :qoute
      t.integer :user_id
      t.integer :created_by
      t.integer :updated_by

      t.timestamps
    end
  end
end
