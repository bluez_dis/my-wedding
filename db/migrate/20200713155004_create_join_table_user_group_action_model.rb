class CreateJoinTableUserGroupActionModel < ActiveRecord::Migration[5.2]
  def change
    create_join_table :user_groups, :action_models do |t|
      t.belongs_to :user_group, index: false
    	t.belongs_to :action_model, index: false
    end
  end
end
