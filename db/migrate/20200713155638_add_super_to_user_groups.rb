class AddSuperToUserGroups < ActiveRecord::Migration[5.2]
  def change
    add_column :user_groups, :super, :integer, default: 0
  end
end
