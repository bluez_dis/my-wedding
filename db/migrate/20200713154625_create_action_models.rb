class CreateActionModels < ActiveRecord::Migration[5.2]
  def change
    create_table :action_models do |t|
      t.string :name
      t.string :path
      t.integer :controller_model_id

      t.timestamps
    end
  end
end
