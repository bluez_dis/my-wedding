class CreateWeedings < ActiveRecord::Migration[5.2]
  def change
    create_table :weedings do |t|
      t.integer :bride_id
      t.integer :groom_id
      t.integer :location_id
      t.date :start_date
      t.date :end_date
      t.integer :start_time
      t.integer :end_time
      t.integer :number_guest
      
      t.timestamps
    end
  end
end
