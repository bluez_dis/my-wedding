ControllerModel.destroy_all

ControllerModel.create(id: 1, name: 'Dashboard', path: "homes")
ControllerModel.create(id: 2, name: 'User', path: "admin/users")
ControllerModel.create(id: 3, name: 'Grup User', path: "admin/user_groups")
ControllerModel.create(id: 4, name: 'Brides', path: "admin/brides")
ControllerModel.create(id: 5, name: 'Grooms', path: "admin/grooms")
ControllerModel.create(id: 6, name: 'Invited Guests', path: "admin/invited_guests")
ControllerModel.create(id: 7, name: 'Weedings', path: "admin/weedings")
