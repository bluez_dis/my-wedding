ActionModel.destroy_all

ControllerModel.all.each do |controller_model|
	ActionModel.create(controller_model_id: controller_model.id, name: "View List", path: "index")
	next if controller_model.path == "homes"
	ActionModel.create(controller_model_id: controller_model.id, name: "View Detail", path: "show")
	ActionModel.create(controller_model_id: controller_model.id, name: "Create New", path: "new")
	ActionModel.create(controller_model_id: controller_model.id, name: "Edit", path: "edit")
	ActionModel.create(controller_model_id: controller_model.id, name: "Destroy", path: "destroy")	
end