Rails.application.routes.draw do
  
  # For details on the DSL available within this file, see http://guides.rubyonrails.org/routing.html
  root 'homes#index'
  get 'admins' => 'admins#index', :as => 'admin_root'
  get 'undangan/:id' => 'bridegrooms#index'
  devise_for :users, :path => '', :path_names => { :sign_in => "login", :sign_out => "logout", :sign_up => "register" }
  namespace :admin do    
    resources :users    
    resources :user_groups
    resources :brides    
    resources :grooms
    resources :invited_guests    
    resources :weedings    
    resources :weeding_photos    
  end

end
